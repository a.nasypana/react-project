import { FC } from 'react';
import { LoginForm } from '../components/forms';


export const LoginPage: FC = () => {
    return (
        <main>
            <section className = 'sign-form'>
                <LoginForm />
            </section>
        </main>
    );
};
