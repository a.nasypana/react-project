import { FC } from 'react';
import { TasksManager } from '../components';

export const TasksManagerPage: FC = () => {
    return (
        <>
            <TasksManager />
        </>
    );
};
