export { ProfilePage } from './profile';
export { LoginPage } from './login';
export { SignUpPage } from './signup';
export { TasksManagerPage } from './managerTasks';
