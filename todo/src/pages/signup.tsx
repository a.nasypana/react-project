import { FC } from 'react';
import { SignUpForm } from '../components/forms';

export const SignUpPage: FC = () => {
    return (
        <main>
            <section className = 'publish-tip sign-form'>
                <SignUpForm />
            </section>
        </main>
    );
};
