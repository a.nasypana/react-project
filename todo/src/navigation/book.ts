export const book = Object.freeze({
    login:       '/todo/login',
    signUp:      '/todo/signup',
    taskManager: '/todo/task-manager',
    profile:     '/todo/profile',
});
