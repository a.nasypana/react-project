import { authApi } from './auth';
import { tasksApi } from './tasks';
import { tagsApi } from './tags';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    getVersion() {
        return '0.0.1';
    },
    ...tagsApi,
    ...authApi,
    ...tasksApi,
});
