import axios from 'axios';

import { ITagModel } from '../types';

const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';
const TAGS_URL = `${TODO_API_URL}/tags`;

export const tagsApi = Object.freeze({
    async getTags(): Promise<ITagModel[]> {
        const { data } = await axios.get<ITagModel[]>(`${TAGS_URL}`);

        return data;
    },
});
