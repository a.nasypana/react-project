import axios, { AxiosResponse } from 'axios';

import { ITaskModel, AuthHeader } from '../types';
import { ITaskFormShape } from '../components/forms/types';

const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';
const TASKS_URL = `${TODO_API_URL}/tasks`;

export const tasksApi = Object.freeze({
    async getTasks(token: string): Promise<ITaskModel[]> {
        const config: AuthHeader = {};
        config.headers = {
            authorization: `Bearer ${token}`,
        };

        const { data } = await axios.get<AxiosResponse<ITaskModel[]>>(`${TASKS_URL}`, config);

        return data.data;
    },
    async getTaskById(id: string, token: string): Promise<ITaskModel> {
        const config: AuthHeader = {};
        config.headers = {
            authorization: `Bearer ${token}`,
        };
        const { data } = await axios.get<AxiosResponse<ITaskModel>>(`${TASKS_URL}/${id}`, config);

        return data.data;
    },
    async createTask(task: ITaskFormShape, token: string): Promise<ITaskModel> {
        const config: AuthHeader = {};
        config.headers = {
            authorization: `Bearer ${token}`,
        };
        const { data } = await axios.post<AxiosResponse<ITaskModel>>(
            `${TASKS_URL}`,
            task,
            config,
        );

        return data.data;
    },
    async updateTask(task: ITaskFormShape, id: string, token: string): Promise<ITaskModel> {
        const config: AuthHeader = {};
        config.headers = {
            authorization: `Bearer ${token}`,
        };
        const { data } = await axios.put<AxiosResponse<ITaskModel>>(
            `${TASKS_URL}/${id}`,
            task,
            config,
        );

        return data.data;
    },
    async deleteTask(id: string, token: string): Promise<void> {
        const config: AuthHeader = {};
        config.headers = {
            authorization: `Bearer ${token}`,
        };
        await axios.delete<AxiosResponse<ITaskModel>>(
            `${TASKS_URL}/${id}`,
            config,
        );
    },
});
