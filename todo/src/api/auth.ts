import axios, { AxiosResponse } from 'axios';

import { ILogin, IProfile } from '../types';
import { ILoginFormShape, ISignUp } from '../components/forms/types';

const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';
const AUTH_URL = `${TODO_API_URL}/auth`;


export const authApi = Object.freeze({
    async getProfile(token: string): Promise<IProfile> {
        const { data: profile } = await axios.get<IProfile>(
            `${AUTH_URL}/profile`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );

        return profile;
    },
    async signUp(user: ISignUp): Promise<ILogin> {
        const { data } = await axios.post<ISignUp, AxiosResponse<ILogin>>(
            `${AUTH_URL}/registration`,
            user,
        );

        return data;
    },
    async login(credentials: ILoginFormShape): Promise<ILogin> {
        const { email, password } = credentials;
        const { data } = await axios.get<ILogin>(
            `${AUTH_URL}/login`,
            {
                headers: {
                    authorization: `Basic ${btoa(`${email}:${password}`)}`,
                },
            },
        );

        return data;
    },
    async logout(token: string): Promise<void> {
        await axios.get<AxiosResponse>(
            `${AUTH_URL}/logout`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            },
        );
    },
});
