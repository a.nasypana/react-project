import * as yup from 'yup';
import { ITaskFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина - ${min} символов';
// eslint-disable-next-line no-template-curly-in-string
const tooLongMessage = 'мaксимальная длина - ${max} символов';
const dateMessage = 'укажите пожалуста дедлайн, он не может быть раньше текущего момента';
const tagMessage = 'вибирете пожалуста тег';

export const schema: yup.SchemaOf<ITaskFormShape> = yup.object().shape({
    completed: yup
        .boolean()
        .required('*'),
    title: yup
        .string()
        .min(3, `Задача: ${tooShortMessage}`)
        .max(64, `Задача: ${tooLongMessage}`)
        .required('*'),
    description: yup
        .string()
        .min(3, `Описание: ${tooShortMessage}`)
        .max(250, `Описание: ${tooLongMessage}`)
        .required('*'),
    deadline: yup
        .date()
        .min(new Date(), dateMessage)
        .required('*'),
    tag: yup
        .string()
        .required(tagMessage),
});
