// Core
import { FC, useEffect } from 'react';
import { useForm, UseFormProps } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Other
import {
    TaskInput, TagsList, DatePickerElement, HeadButton,
} from '../elements';
import { useManageTask } from '../../../hooks';
import { schema } from './config';
import { ITaskFormShape } from '../types';


const titlePlacholder = 'Пройти интенсив по React + Redux + TS + Mobx';
const descPlacholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.';

export const TaskForm: FC = () => {
    const {
        task, isFetching, handlerTaskSubmit, closeCard, restData, removeTask,
    } = useManageTask();

    const formConfig: UseFormProps = {
        mode:          'onTouched',
        resolver:      yupResolver(schema),
        defaultValues: {
            completed:   task.completed,
            title:       task.title,
            description: task.description,
            deadline:    new Date(task.deadline),
            tag:         task.tag.id,
        },
    };

    const {
        handleSubmit, control, register, setValue, reset, clearErrors,
        formState: { isValid, touchedFields, errors },
    } = useForm(formConfig);

    // When choose new task, fields of form don`t update  in form. So useEffeect was used
    useEffect(() => {
        setValue('completed', task.completed);
        setValue('title', task.title);
        setValue('description', task.description);
        setValue('deadline', new Date(task.deadline));
        setValue('tag', task.tag.id);
    }, [task]);

    const isHide = touchedFields?.title || touchedFields?.description || touchedFields?.completed
    || touchedFields.tag || touchedFields.deadline;

    const onSubmit = handleSubmit(async (data: ITaskFormShape) => {
        if (isValid) {
            await handlerTaskSubmit(data);
        }
    });

    return (
        <form onSubmit = { onSubmit }>
            <div className = 'head'>
                {
                    !!task?.id
                    && <HeadButton
                        register = { register('completed') }
                        control = { control }
                        isFetching = { isFetching } />
                }
                { !!task?.id
                    && <div
                        onClick = { removeTask }
                        className = 'button-remove-task'></div>
                }
            </div>
            <fieldset className = 'fieldset' disabled = { isFetching }>
                <div className = 'content'>
                    <TaskInput
                        label = 'Задачи'
                        placeholder = { titlePlacholder }
                        register = { register('title') } />
                    <DatePickerElement
                        register = { register('deadline') }
                        control = { control } />
                    <div className = 'description'>
                        <TaskInput
                            label = 'Описание'
                            tag = 'textarea'
                            placeholder = { descPlacholder }
                            register = { register('description') } />
                    </div>
                    <TagsList
                        control = { control }
                        register = { register('tag') } />
                    <div className = 'errors'>
                        <p className = 'errorMessage'> { errors?.title?.message }</p>
                        <p className = 'errorMessage'> { errors?.deadline?.message }</p>
                        <p className = 'errorMessage'> { errors?.description?.message }</p>
                        <p className = 'errorMessage'> { errors?.tag?.message }</p>
                    </div>
                    <div className = 'form-controls'>
                        <button
                            disabled = { !isHide }
                            onClick = { restData(reset, clearErrors) }
                            type = 'reset'
                            className = 'button-reset-task' >
                            Reset
                        </button>
                        <button
                            className = 'button-save-task'
                            disabled = { !isHide }
                            type = 'submit' >
                            Save
                        </button>
                        <div className = 'wrapper-close'>
                            <button
                                onClick = { closeCard }
                                className = 'button-close-task' >
                                Close
                            </button>
                        </div>

                    </div>
                </div>
            </fieldset>
        </form>
    );
};
