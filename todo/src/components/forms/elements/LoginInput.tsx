import { FC } from 'react';
import { UseFormRegisterReturn } from 'react-hook-form';

interface IPropsTypes {
    placeholder: string;
    lang?: string;
    type?: string;
    register?: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
}

export const LoginInput: FC<IPropsTypes> = (props) => {
    return (
        <label className = 'label'>
            <span className = 'error-message'>{ props?.error?.message }</span>
            <input
                placeholder = { props.placeholder }
                type = { props.type }
                lang = { props?.lang }
                { ...props?.register } />
        </label>
    );
};

LoginInput.defaultProps = {
    type: 'text',
};
