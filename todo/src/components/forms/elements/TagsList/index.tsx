// Core
import { FC } from 'react';
import {
    UseFormRegisterReturn, Controller, Control, FieldValues,
} from 'react-hook-form';

// Other
import { useTags } from '../../../../hooks';
import { TagElement } from './TagElement';


interface IPropsTypes {
    register: UseFormRegisterReturn;
    control: Control<FieldValues>;
}

export const TagsList: FC<IPropsTypes> = (props) => {
    const { register, control } = props;
    const { tags } = useTags();

    return (
        <Controller
            control = { control }
            name = { register?.name || '' }
            render = { ({ field }) => {
                const tagsJSX = tags?.map((tag) => <TagElement
                    setTag = { (id: string) => {
                        field.onBlur();
                        field.onChange(id);
                    } }
                    selectedId = { field.value }
                    key = { tag?.id } { ...tag } />);

                return (
                    <div className = 'tags'>
                        { tagsJSX }
                    </div>
                );
            } } />
    );
};
