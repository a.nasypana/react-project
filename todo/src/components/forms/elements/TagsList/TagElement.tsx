// Core
import { FC } from 'react';
import cn from 'classnames';

// Other
import { ITagModel } from '../../../../types';


interface IProps extends ITagModel {
    setTag: (id: string) => void;
    selectedId: string;
}


export const TagElement: FC<IProps> = (props) => {
    const {
        name, color, bg, id, selectedId, setTag,
    } = props;

    const styles = cn('tag', { selected: selectedId === id });

    const clickHandler = () => {
        setTag(id);
    };

    return (
        <span
            className = { styles }
            style = { { color, backgroundColor: bg } }
            onClick = { clickHandler } >
            { name }
        </span>
    );
};
