import { FC } from 'react';
import {
    UseFormRegisterReturn, Control, Controller, FieldValues,
} from 'react-hook-form';
import DatePicker, { registerLocale } from 'react-datepicker';
import { ru } from 'date-fns/locale';


// What
interface IPropsTypes {
    register: UseFormRegisterReturn;
    control: Control<FieldValues>;
}

export const DatePickerElement: FC<IPropsTypes> = (props) => {
    const { control, register } = props;
    registerLocale('ru', ru);

    return (
        <Controller
            name = { register?.name  || '' }
            control = { control }
            render = { ({ field }) => (
                <div className = 'deadline'>
                    <span className = 'label'>Дедлайн</span>
                    <span className = 'date'>
                        <DatePicker
                            locale = 'ru'
                            dateFormat = 'dd MMM yyyy'
                            minDate  = { new Date() }
                            selected = { field.value }
                            onChange = { (date) => {
                                field.onBlur();
                                field.onChange(date);
                            }  } />
                    </span>
                </div>
            ) } />
    );
};
