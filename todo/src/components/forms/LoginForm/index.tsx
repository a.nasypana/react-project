// Core
import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Other
import { LoginInput } from '../elements';
import { useAuth } from '../../../hooks';
import { schema } from './config';
import { ILoginFormShape } from '../types';
import { book } from '../../../navigation/book';

export const LoginForm: FC = () => {
    const { isFetching, loginHandler } = useAuth();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ILoginFormShape) => {
        await loginHandler(credentials);
        form.reset();
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset disabled = { isFetching }>
                <legend>Вход</legend>
                <LoginInput
                    placeholder = 'Электропочта'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <LoginInput
                    type = 'password'
                    placeholder = 'Пароль'
                    lang = 'en'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />
                <input
                    className = 'button-login'
                    type = 'submit'
                    value = 'Войти' />
            </fieldset>
            <p>Если у вас до сих пор нет учётной записи,
                вы можете <Link to = { book.signUp }>зарегистрироваться</Link>.
            </p>
        </form>
    );
};
