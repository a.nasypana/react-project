import * as yup from 'yup';
import { ISignUpFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'минимальная длина - ${min} символов';
// eslint-disable-next-line no-template-curly-in-string
const tooLongMessage = 'мaксимальная длина - ${max} символов';

export const schema: yup.SchemaOf<ISignUpFormShape> = yup.object().shape({
    name: yup
        .string()
        .required('*')
        .min(3, tooShortMessage)
        .max(40, tooLongMessage),
    email: yup
        .string()
        .email()
        .required('*'),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(16, tooLongMessage)
        .required('*'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Парали должны совпадать')
        .required('*'),
});
