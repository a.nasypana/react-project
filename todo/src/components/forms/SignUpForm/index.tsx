// Core
import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Other
import { LoginInput } from '../elements';
import { schema } from './config';
import { ISignUpFormShape } from '../types';
import { useAuth } from '../../../hooks';
import { book } from '../../../navigation/book';

export const SignUpForm: FC = () => {
    const { isFetching, registrationHandler } = useAuth();
    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: ISignUpFormShape) => {
        const { confirmPassword, ...newUser } = data;
        await registrationHandler(newUser);
        form.reset();
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset disabled = { isFetching }>
                <legend>Регистрация</legend>
                <LoginInput
                    placeholder = 'Имя и фамилия'
                    error = { form.formState.errors.name }
                    register = { form.register('name') } />
                <LoginInput
                    placeholder = 'Электропочта'
                    lang = 'en'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <LoginInput
                    type = 'password'
                    placeholder = 'Пароль'
                    lang = 'en'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />
                <LoginInput
                    type = 'password'
                    placeholder = 'Подтверждение пароля'
                    lang = 'en'
                    error = { form.formState.errors.confirmPassword }
                    register = { form.register('confirmPassword') } />
                <input
                    className = 'button-login'
                    type = 'submit'
                    value = 'Зарегистрироваться' />
            </fieldset>
            <p>Перейти к
                <Link to = { book.login }> логину</Link>.
            </p>
        </form>
    );
};
