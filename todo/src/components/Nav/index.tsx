// Core
import { FC } from 'react';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';

import { book } from '../../navigation/book';
import { useAuth } from '../../hooks';

export const Navigation: FC = () => {
    const { logout, token } = useAuth();
    const styles = cn({ 'disabled-link': !token });

    return (
        <nav>
            { !token && <NavLink to = { book.login }>Войти</NavLink> }
            <NavLink
                className = { styles }
                to = { token && book.taskManager  } >К задачам</NavLink>
            <NavLink
                className = { styles }
                to = { token && book.profile } >Профиль</NavLink>
            { !!token
                && <button
                    className = 'button-logout'
                    onClick = { logout } >
                    Выйти
                </button>
            }
        </nav>
    );
};
