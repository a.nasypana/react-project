import { FC } from 'react';
import cn from 'classnames';

import { ITaskModel } from '../../types';
import { strToData } from '../../utils/date';

interface IProps extends ITaskModel {
    clickHandler: () => void;
}

export const Task:FC<IProps> = (props) => {
    const {
        completed, title,  deadline, tag: { name, bg, color }, clickHandler,
    } = props;
    const styles = cn('task', { completed });

    return (
        <div
            onClick = { clickHandler }
            className = { styles }>
            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ strToData(deadline) }</span>
                <span
                    style = { { color, backgroundColor: bg } }
                    className = 'tag'>
                    { name }
                </span>
            </div>
        </div>
    );
};
