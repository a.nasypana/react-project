import { FC } from 'react';
import cn from 'classnames';
import Audio from  'react-loader-spinner';

import { ITaskModel } from '../../types';
import { Task } from './Task';

interface IProps {
    tasks: ITaskModel[];
    isFetching: boolean;
    selectTask: (id: string) => () => void;
}

export const TasksList: FC<IProps> = (props) => {
    const { tasks, isFetching, selectTask } = props;
    const styles = cn('list', { empty: !isFetching && tasks.length === 0 });

    const tasksJSX = tasks?.map((task) => <Task
        key = { task?.id } clickHandler = { selectTask(task?.id) }
        { ...task } />);


    return (
        <div className = { styles }>
            <div className = 'tasks'>
                { isFetching
                    && <Audio
                        color = '#4D7CFE'
                        height = '80'
                        width = '80'
                        type = 'Audio' />
                }
                { !isFetching && tasksJSX }
            </div>
        </div>
    );
};

