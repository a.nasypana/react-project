import { FC } from 'react';
import { TaskForm } from '../forms';


export const TaskCard: FC = () => {
    return (
        <div className = 'task-card'>
            <TaskForm />
        </div>
    );
};
