import { FC } from 'react';
import Audio from  'react-loader-spinner';

import { useTasks } from '../../hooks';
import { TaskCard } from './TaskCard';
import { TasksList } from './TasksList';


export const TasksManager:FC = () => {
    const {
        isFetching, isShowTaskCard, openNewTask, tasks, selectTask, isFetchingTags,
    } = useTasks();

    const isLoading = isFetching || isFetchingTags;

    return (
        <main>
            <div className = 'controls'>
                <i className = 'las'></i>
                <button
                    onClick = { openNewTask }
                    disabled = { isLoading }
                    className = 'button-create-task'>
                    Новая задача
                </button>
            </div>
            <div className = 'wrap'>
                {
                    isLoading && <Audio
                        color = '#4D7CFE'
                        height = '100'
                        width = '100'
                        type = 'Audio' />
                }
                { !isLoading
                    && <TasksList
                        tasks = { tasks }
                        selectTask = { selectTask }
                        isFetching = { isFetching } /> }
                { !isLoading && isShowTaskCard && <TaskCard /> }
            </div>
        </main>
    );
};
