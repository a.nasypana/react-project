import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const strToData = (str: string): string => format(new Date(str), 'dd MMM yyyy', { locale: ru });
