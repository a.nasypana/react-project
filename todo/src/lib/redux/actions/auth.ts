import { AxiosError } from 'axios';
import { authTypes } from '../types';
import { api } from '../../../api';
import { AppThunk } from '../init/store';
import { IProfile, IMessage, IErrorResponse } from '../../../types';
import { ILoginFormShape, ISignUp } from '../../../components/forms/types';

export const authActions = Object.freeze({
    startFetching: () => {
        return {
            type: authTypes.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: authTypes.STOP_FETCHING,
        };
    },
    setProfile: (profile: IProfile) => {
        return {
            type:    authTypes.SET_PROFILE,
            payload: profile,
        };
    },
    setMessage: (message: IMessage) => {
        return {
            type:    authTypes.SET_MESSAGE,
            payload: message,
        };
    },
    setToken: (token: string) => {
        return {
            type:    authTypes.SET_TOKEN,
            payload: token,
        };
    },
    setSuccess: (isSuccess: boolean) => {
        return {
            type:    authTypes.SET_SUCCESS,
            payload: isSuccess,
        };
    },
    resetMessage: () => {
        return {
            type: authTypes.RESET_MESSAGE,
        };
    },
    loginAsync: (credentials: ILoginFormShape): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const { data } = await api.login(credentials);

            dispatch(authActions.setToken(data));

            dispatch(authActions.setSuccess(true));

            dispatch(authActions.setMessage({
                message: 'Добро пожаловать!',
                type:    'success',
            }));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(authActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    signUpAsync: (user: ISignUp): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const { data } = await api.signUp(user);

            dispatch(authActions.setToken(data));

            dispatch(authActions.setSuccess(true));

            dispatch(authActions.setMessage({
                message: `Добро пожаловать, ${user?.name}!`,
                type:    'success',
            }));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(authActions.setSuccess(false));

            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    fetchProfileAsync: (token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());
            const profile = await api.getProfile(token);

            dispatch(authActions.setProfile(profile));

            dispatch(authActions.setSuccess(true));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(authActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },
    logoutAsync: (token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(authActions.startFetching());

            await api.logout(token);

            dispatch(authActions.setToken(''));

            localStorage.setItem('token', '');

            dispatch(authActions.setSuccess(true));

            dispatch(authActions.setMessage({
                message: 'Возврощайся по скорее:)',
                type:    'info',
            }));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(authActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(authActions.stopFetching());
        }
    },

});
