import { AxiosError } from 'axios';
import { tasksTypes } from '../types';
import { api } from '../../../api';
import { AppThunk } from '../init/store';
import { ITaskModel, IErrorResponse } from '../../../types';
import { authActions } from './auth';
import { ITaskFormShape } from '../../../components/forms/types';


export const tasksActions = Object.freeze({
    startFetching: () => {
        return {
            type: tasksTypes.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: tasksTypes.STOP_FETCHING,
        };
    },
    setTasks: (tasks: ITaskModel[]) => {
        return {
            type:    tasksTypes.SET_TASKS,
            payload: tasks,
        };
    },
    selectTask: (task: ITaskModel) => {
        return {
            type:    tasksTypes.SELECT_TASK,
            payload: task,
        };
    },
    resetTask: () => {
        return {
            type: tasksTypes.RESET_TASK,
        };
    },
    setSuccess: (isSuccess: boolean) => {
        return {
            type:    tasksTypes.SET_SUCCESS,
            payload: isSuccess,
        };
    },
    fetchTasksAsync: (token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(tasksActions.startFetching());
            const tasks = await api.getTasks(token);

            dispatch(tasksActions.setTasks(tasks));

            dispatch(tasksActions.setSuccess(true));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tasksActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tasksActions.stopFetching());
        }
    },
    fetchTaskByIdAsync: (id: string, token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(tasksActions.startFetching());
            const task = await api.getTaskById(id, token);

            dispatch(tasksActions.selectTask(task));

            dispatch(tasksActions.setSuccess(true));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tasksActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tasksActions.stopFetching());
        }
    },
    createTaskAsync: (task: ITaskFormShape, token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(tasksActions.startFetching());
            const data = await api.createTask(task, token);

            dispatch(tasksActions.selectTask(data));

            dispatch(authActions.setMessage({
                message: `Задача - ${data.title} успешно создана.`,
                type:    'info',
            }));

            dispatch(tasksActions.fetchTasksAsync(token));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tasksActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tasksActions.stopFetching());
        }
    },
    updateTaskAsync: (
        task: ITaskFormShape, id: string, token: string,
    ): AppThunk => async (dispatch) => {
        try {
            dispatch(tasksActions.startFetching());
            const data = await api.updateTask(task, id, token);

            dispatch(tasksActions.selectTask(data));

            dispatch(authActions.setMessage({
                message: `Задача - ${data.title} успешно обновлена.`,
                type:    'info',
            }));

            dispatch(tasksActions.fetchTasksAsync(token));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tasksActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tasksActions.stopFetching());
        }
    },
    deleteTaskAsync: (id: string, token: string): AppThunk => async (dispatch) => {
        try {
            dispatch(tasksActions.startFetching());
            await api.deleteTask(id, token);

            dispatch(authActions.setMessage({
                message: `Задача - ${id} успешно удалена.`,
                type:    'info',
            }));

            dispatch(tasksActions.fetchTasksAsync(token));

            dispatch(tasksActions.resetTask());
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tasksActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tasksActions.stopFetching());
        }
    },
});
