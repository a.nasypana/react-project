import { AxiosError } from 'axios';
import { tagsTypes } from '../types';
import { api } from '../../../api';
import { AppThunk } from '../init/store';
import { ITagModel, IErrorResponse } from '../../../types';
import { authActions } from './auth';


export const tagsActions = Object.freeze({
    startFetching: () => {
        return {
            type: tagsTypes.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: tagsTypes.STOP_FETCHING,
        };
    },
    setTags: (tags: ITagModel[]) => {
        return {
            type:    tagsTypes.SET_TAGS,
            payload: tags,
        };
    },
    setSuccess: (isSuccess: boolean) => {
        return {
            type:    tagsTypes.SET_SUCCESS,
            payload: isSuccess,
        };
    },
    fetchTagsAsync: (): AppThunk => async (dispatch) => {
        try {
            dispatch(tagsActions.startFetching());
            const tags = await api.getTags();

            dispatch(tagsActions.setTags(tags));

            dispatch(tagsActions.setSuccess(true));
        } catch (error) {
            const { response }  = error as AxiosError<IErrorResponse>;
            const message = response?.data?.message || 'Что то пошло не так.';

            dispatch(tagsActions.setSuccess(false));
            dispatch(authActions.setMessage({
                message,
                type: 'error',
            }));
        } finally {
            dispatch(tagsActions.stopFetching());
        }
    },
});
