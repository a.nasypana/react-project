import { RootState } from '../init/store';
import { ITaskModel } from '../../../types';
import { IInitialState } from '../reducers/tasks';

export const getSelectedTask = (state: RootState): ITaskModel => {
    return state.tasks.selectedTask;
};

export const getArrayOfTasks = (state: RootState): ITaskModel[] => {
    return state.tasks.tasks;
};

export const getTasks = (state: RootState): IInitialState => {
    return state.tasks;
};
