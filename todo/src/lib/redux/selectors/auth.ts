import { RootState } from '../init/store';
import { IProfile, IMessage } from '../../../types';
import { initialState } from '../reducers/auth';

export const getToken = (state: RootState): string => {
    return state.auth.token;
};

export const getMessage = (state: RootState): IMessage => {
    return state.auth.message;
};

export const getProfile = (state: RootState): IProfile => {
    return state.auth.profile;
};

export const getAuth = (state: RootState): typeof initialState => {
    return state.auth;
};
