import { RootState } from '../init/store';
import { initialState } from '../reducers/tags';


export const getTags = (state: RootState): typeof initialState => {
    return state.tags;
};
