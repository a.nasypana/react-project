// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth,
    tagsReducer as tags,
    tasksReducer as tasks,
} from '../reducers';


export const rootReducer = combineReducers({
    auth,
    tasks,
    tags,
});
