import { AnyAction } from 'redux';
import { tasksTypes } from '../types/tasks';
import { ITaskModel, TagEnum } from '../../../types';

export interface IInitialState {
    tasks: ITaskModel[];
    selectedTask: null | ITaskModel;
    isFetching: boolean;
    isSuccess: boolean | undefined;
}

const initialState: IInitialState = {
    tasks:        new Array<ITaskModel>(),
    selectedTask: {
        id:          '',
        completed:   false,
        title:       '',
        description: '',
        deadline:    new Date().toISOString(),
        tag:         {
            id:    '',
            name:  TagEnum.SKETCH,
            color: '',
            bg:    '',
        },
    },
    isFetching: false,
    isSuccess:  undefined,
};

export const tasksReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;
    switch (type) {
        case tasksTypes.STOP_FETCHING: {
            return {
                ...state,
                isFetching: false,
            };
        }

        case tasksTypes.START_FETCHING: {
            return {
                ...state,
                isFetching: true,
                isSuccess:  undefined,
            };
        }

        case tasksTypes.SET_TASKS: {
            return {
                ...state,
                tasks: [...payload],
            };
        }

        case tasksTypes.SELECT_TASK: {
            return {
                ...state,
                selectedTask: { ...payload },
            };
        }

        case tasksTypes.RESET_TASK: {
            return {
                ...state,
                selectedTask: null,
            };
        }

        case tasksTypes.SET_SUCCESS: {
            return {
                ...state,
                isSuccess: payload,
            };
        }

        default: {
            return state;
        }
    }
};
