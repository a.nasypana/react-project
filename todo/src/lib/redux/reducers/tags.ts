import { AnyAction } from 'redux';
import { tagsTypes } from '../types/tags';
import { ITagModel } from '../../../types';

interface IInitialState {
    tags: ITagModel[];
    isFetching: boolean;
    isSuccess: boolean | undefined;
}

export const initialState: IInitialState = {
    tags:       new Array<ITagModel>(),
    isFetching: false,
    isSuccess:  undefined,
};

export const tagsReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;
    switch (type) {
        case tagsTypes.STOP_FETCHING: {
            return {
                ...state,
                isFetching: false,
            };
        }

        case tagsTypes.START_FETCHING: {
            return {
                ...state,
                isFetching: true,
                isSuccess:  undefined,
            };
        }

        case tagsTypes.SET_TAGS: {
            return {
                ...state,
                tags: [...payload],
            };
        }

        case tagsTypes.SET_SUCCESS: {
            return {
                ...state,
                isSuccess: payload,
            };
        }

        default: {
            return state;
        }
    }
};
