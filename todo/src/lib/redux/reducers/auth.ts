import { AnyAction } from 'redux';
import { authTypes } from '../types/auth';
import { IMessage, IProfile } from '../../../types';

interface IInitialState {
    token: string;
    message: IMessage;
    profile: IProfile | null;
    isSuccess: undefined | boolean;
    isFetching: boolean;
}

export const initialState: IInitialState = {
    token:   '',
    message: {
        message: '',
        type:    'info',
    },
    profile:    null,
    isSuccess:  undefined,
    isFetching: false,
};

export const authReducer = (state = initialState, action: AnyAction) => {
    const { type, payload } = action;
    switch (type) {
        case authTypes.STOP_FETCHING: {
            return {
                ...state,
                isFetching: false,
            };
        }

        case authTypes.START_FETCHING: {
            return {
                ...state,
                isFetching: true,
                isSuccess:  undefined,
            };
        }

        case authTypes.SET_TOKEN: {
            return {
                ...state,
                token: payload,
            };
        }

        case authTypes.SET_MESSAGE: {
            return {
                ...state,
                message: { ...payload },
            };
        }

        case authTypes.RESET_MESSAGE: {
            return {
                ...state,
                message: {
                    message: '',
                    type:    'info',
                },
            };
        }

        case authTypes.SET_PROFILE: {
            return {
                ...state,
                profile: { ...payload },
            };
        }

        case authTypes.SET_SUCCESS: {
            return {
                ...state,
                isSuccess: payload,
            };
        }

        default: {
            return state;
        }
    }
};
