export const tagsTypes = Object.freeze({
    START_FETCHING: '[TAGS] - START_FETCHING',
    STOP_FETCHING:  '[TAGS] - STOP_FETCHING',
    SET_SUCCESS:    '[TAGS] - SET_SUCCESS',
    SET_TAGS:       '[TAGS] - SET_TAGS',

    // Async
    GET_TAGS_ASYNC: '[TAGS] - GET_TAGS_ASYNC',
});
