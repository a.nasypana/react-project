export const tasksTypes = Object.freeze({
    START_FETCHING: '[TASKS] - START_FETCHING',
    STOP_FETCHING:  '[TASKS] - STOP_FETCHING',
    SET_TASKS:      '[TASKS] - SET_TASKS',
    SELECT_TASK:    '[TASKS] - SELECT_TASK',
    RESET_TASK:     '[TASKS] - RESET_TASK',
    SET_SUCCESS:    '[TASKS] - SET_SUCCESS',

    // Async
    GET_TASKS_ASYNC:   '[TASKS] - GET_TASKS_ASYNC',
    GET_TASK_ASYNC:    '[TASKS] - GET_TASK_ASYNC',
    CREATE_TASK_ASYNC: '[TASKS] - CREATE_TASK_ASYNC',
    UPDATE_TASK_ASYNC: '[TASKS] - UPDATE_TASK_ASYNC',
    DELETE_TASK_ASYNC: '[TASKS] - DELETE_TASK_ASYNC',
});
