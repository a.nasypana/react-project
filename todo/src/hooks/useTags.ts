// Core
import { useSelector } from 'react-redux';

import { getTags } from '../lib/redux/selectors';

export const useTags = () => {
    const { tags, isFetching } = useSelector(getTags);

    return {
        tags,
        isFetching,
    };
};
