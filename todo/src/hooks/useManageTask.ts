// Core
import { useSelector } from 'react-redux';

// Other
import { ITaskFormShape } from '../components/forms/types';
import { useAppDispatch } from '../lib/redux/init/store';
import { tasksActions } from '../lib/redux/actions';
import { getTasks, getToken } from '../lib/redux/selectors';
import { newTask } from './useTasks';


export const useManageTask = () => {
    const dispatch = useAppDispatch();
    const token = useSelector(getToken);

    const { isFetching, selectedTask } = useSelector(getTasks);

    const handlerTaskSubmit = (data: ITaskFormShape) => {
        if (selectedTask?.id) {
            dispatch(tasksActions.updateTaskAsync(data, selectedTask.id, token));
        } else {
            dispatch(tasksActions.createTaskAsync(data, token));
        }
    };

    const removeTask = () => {
        if (selectedTask?.id) {
            dispatch(tasksActions.deleteTaskAsync(selectedTask.id, token));
        }
    };

    const restData = (reset: () => void, clear: () => void) => () => {
        if (selectedTask?.id) {
            reset();
        } else {
            dispatch(tasksActions.selectTask({ ...newTask }));
        }
        clear();
    };

    const closeCard = () => dispatch(tasksActions.resetTask());

    return {
        task: selectedTask || newTask,
        isFetching,
        handlerTaskSubmit,
        restData,
        closeCard,
        removeTask,
    };
};
