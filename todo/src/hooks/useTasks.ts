// Core
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { getTasks, getToken, getTags } from '../lib/redux/selectors';
import { tasksActions, tagsActions } from '../lib/redux/actions';
import { useAppDispatch } from '../lib/redux/init/store';
import { TagEnum } from '../types';


export const newTask = {
    id:          '',
    completed:   false,
    title:       '',
    description: '',
    deadline:    new Date().toISOString(),
    tag:         {
        id:    '',
        name:  TagEnum.SKETCH,
        color: '',
        bg:    '',
    },
};

export const useTasks = () => {
    const dispatch = useAppDispatch();
    const { tasks, isFetching, selectedTask } = useSelector(getTasks);
    const { isFetching: isFetchingTags } = useSelector(getTags);
    const token = useSelector(getToken);

    const openNewTask = () => dispatch(tasksActions.selectTask(newTask));

    const selectTask = (id: string) => () => dispatch(tasksActions.fetchTaskByIdAsync(id, token));

    useEffect(() => {
        if (token) {
            dispatch(tagsActions.fetchTagsAsync());
            dispatch(tasksActions.fetchTasksAsync(token));
        }
    }, [token]);

    return {
        tasks,
        isFetching,
        isFetchingTags,
        selectTask,
        openNewTask,
        isShowTaskCard: !!selectedTask,
    };
};
