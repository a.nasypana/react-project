// Core
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// Other
import { useAppDispatch } from '../lib/redux/init/store';
import { authActions } from '../lib/redux/actions';
import { getAuth } from '../lib/redux/selectors';
import { book } from '../navigation/book';
import { ILoginFormShape, ISignUp } from '../components/forms/types';


export const useAuth = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const { token, isSuccess, isFetching } = useSelector(getAuth);

    const tokenStorage = localStorage.getItem('token') || '';

    const combToken = token || tokenStorage;

    const logout = () => dispatch(authActions.logoutAsync(token));
    const registrationHandler = (data: ISignUp) => dispatch(authActions.signUpAsync(data));
    const loginHandler = (data: ILoginFormShape) => dispatch(authActions.loginAsync(data));

    useEffect(() => {
        if (combToken) {
            dispatch(authActions.fetchProfileAsync(combToken));
        } else {
            navigate(book.login);
        }
    }, [combToken]);

    useEffect(() => {
        if (isSuccess && !!combToken) {
            if (combToken !== token) {
                dispatch(authActions.setToken(combToken));
            }
            if (combToken !== tokenStorage) {
                localStorage.setItem('token', combToken);
            }
            navigate(book.taskManager);
        } else if (isSuccess === false && !!combToken) {
            navigate(book.login);
        }
    }, [isSuccess]);

    return {
        logout,
        registrationHandler,
        loginHandler,
        token,
        isFetching,
    };
};
