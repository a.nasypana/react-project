// Core
import { FC } from 'react';
import {
    Navigate, Outlet, Route, Routes,
} from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';

// Components
import {
    LoginPage, SignUpPage, TasksManagerPage, ProfilePage,
} from './pages';
import { Navigation } from './components';


// Instruments
import { useToastMessage } from './hooks';
import { book } from './navigation/book';


export const App: FC = () => {
    useToastMessage();

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <Navigation />
            <Routes>
                <Route path = '/' element = { <Outlet /> }>
                    <Route path = { book.taskManager } element = { <TasksManagerPage /> } />
                    <Route path = { book.profile } element = { <ProfilePage /> } />
                    <Route path = { book.login } element = { <LoginPage /> } />
                    <Route path = { book.signUp } element = { <SignUpPage /> } />
                </Route>
                <Route
                    path = '*'
                    element = { <Navigate to = { book.taskManager } replace /> } />

            </Routes>
        </>
    );
};
