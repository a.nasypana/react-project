export interface ILogin {
    data: string;
}

export type AuthHeader = {
    headers?: {
        authorization: string;
    }
};

export type ToastTheme = 'error' | 'success' | 'info' | 'warn';

export interface IMessage {
    message: string;
    type: ToastTheme;
}

export interface IErrorResponse {
    error: string;
    message: string;
    statusCode: number;
}

