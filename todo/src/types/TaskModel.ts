import { ITagModel } from './TagModel';


export interface ITaskModel {
    id: string;
    completed: boolean;
    title: string;
    description: string;
    deadline: string;
    created?: string;
    tag: ITagModel;
}
