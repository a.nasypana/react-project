export enum TagEnum {
    SPOTIFY = 'Spotify',
    SKETCH = 'Sketch',
    DRIBBLE = 'Dribble',
    BEHANCE = 'Behance',
    UX = 'UX',
}

export interface ITagModel {
    id:   string;
    name: TagEnum;
    color: string;
    bg: string;
}
