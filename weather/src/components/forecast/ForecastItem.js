import { observer } from 'mobx-react-lite';
import cn from 'classnames';
import { useStore } from '../../hooks';
import { getDayOfWeek } from '../../helpers/dataHelpers';

export const ForecastItem = observer(({
    id, day, type, temperature,
}) => {
    const WeatherStore = useStore();
    const styles = cn({
        day:      true,
        cloudy:   type === 'cloudy',
        rainy:    type === 'rainy',
        sunny:    type === 'sunny',
        selected: id === WeatherStore.selectedDayId,
    });


    return (
        <div
            onClick = { () => WeatherStore.setSelectedDayId(id) }
            className = { styles }>
            <p>{ getDayOfWeek(day) }</p>
            <span>{ temperature }</span>
        </div>
    );
});
