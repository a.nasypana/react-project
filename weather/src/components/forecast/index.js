import { observer } from 'mobx-react-lite';
import { useGetDays, useSelectedDays } from '../../hooks';
import { ForecastItem } from './ForecastItem';


export const Forecast = observer(() => {
    const { isLoading, days } = useGetDays();
    const { showDays } = useSelectedDays(days);


    const itemsJSX = showDays?.map((day) => <ForecastItem key = { day?.id } { ...day } />);

    if (isLoading || showDays?.length === 0) {
        return (
            <div className = 'forecast'></div>
        );
    }

    return (
        <div className = 'forecast'>
            { itemsJSX }
        </div>
    );
});
