import { observer } from 'mobx-react-lite';
import { useGetDays, useSelectedDays } from '../../hooks';


export const CurrentWeather = observer(() => {
    const { days, isLoading, isError } = useGetDays();
    const { day, showDays } = useSelectedDays(days);

    if (isLoading) {
        return (
            <div className = 'current-weather'>
                <h2>Loading..</h2>
            </div>
        );
    }

    if (isLoading) {
        return (
            <div className = 'current-weather'>
                <h2>Something went wrong!</h2>
            </div>
        );
    }

    if (showDays?.length === 0) {
        return (
            <div className = 'current-weather'>
                <h2>За заданими критеріями дані відсутні!</h2>
            </div>
        );
    }

    return (
        <div className = 'current-weather'>
            <p className = 'temperature'>{ day?.temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>{ `%${day?.rain_probability}` }</span>
                <span className = 'humidity'>{ `%${day?.humidity}` }</span>
            </p>
        </div>
    );
});
