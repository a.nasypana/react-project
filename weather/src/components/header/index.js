import cn from 'classnames';
import { observer } from 'mobx-react-lite';
import { useGetDays, useSelectedDays } from '../../hooks';
import { getData, getDayOfWeek } from '../../helpers/dataHelpers';


export const Header = observer(() => {
    const { days, isLoading }  = useGetDays();
    const { day, showDays } = useSelectedDays(days);

    const styles = cn({
        icon:   true,
        cloudy: day?.type === 'cloudy',
        rainy:  day?.type === 'rainy',
        sunny:  day?.type === 'sunny',
    });

    if (isLoading || showDays?.length === 0) {
        return (
            <div className = 'head'></div>
        );
    }

    return (
        <div className = 'head'>
            <div className = { styles }></div>
            <div className = 'current-date'>
                <p>{ getDayOfWeek(day?.day) }</p>
                <span>{ getData(day?.day) }</span>
            </div>
        </div>
    );
});
