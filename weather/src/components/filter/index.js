import { observer } from 'mobx-react-lite';
import { useForm } from 'react-hook-form';

import { useStore, useGetDays } from '../../hooks';
import { CustomInput } from './CustomInput';

export const Filter = observer(() => {
    const WeatherStore = useStore();
    const { days } = useGetDays();
    const btnName = WeatherStore.isFiltered ? 'скасувати' : 'відфільтрувати';

    const form = useForm({
        mode: 'onTouched',
    });

    const onSubmit = form.handleSubmit(
        (data) => {
            if (WeatherStore.isFiltered) {
                WeatherStore.resetFilter();
                if (days?.length > 0) {
                    WeatherStore.setSelectedDayId(days[ 0 ]?.id);
                }

                return;
            }
            WeatherStore.applyFilter(data);
        },
    );


    return (
        <form className = 'filter' onSubmit = { onSubmit } >
            <fieldset disabled = { WeatherStore.isFiltered }>
                <CustomInput
                    className = { WeatherStore.type === 'cloudy' ? 'checkbox selected' : 'checkbox' }
                    type = 'radio'
                    value = 'cloudy'
                    label = 'Хмарно'
                    register = { form.register('type', {
                        onChange: (event) => WeatherStore.setType(event.target.value),
                    }) } />
                <CustomInput
                    className = { WeatherStore.type === 'sunny' ? 'checkbox selected' : 'checkbox' }
                    type = 'radio'
                    value = 'sunny'
                    label = 'Сонячно'
                    register = { form.register('type') } />
                <CustomInput
                    value = { WeatherStore.minTemperature }
                    label = 'Min температура'
                    register = { form.register('minTemperature', {
                        onChange: (event) => WeatherStore.setMinTemperature(event.target.value),
                    }) } />
                <CustomInput
                    label = 'Max температура'
                    value = { WeatherStore.maxTemperature }
                    register = { form.register('maxTemperature', {
                        onChange: (event) => {
                            WeatherStore.setMaxTemperature(event.target.value);
                        },
                    }) } />

            </fieldset>
            <button
                disabled = { WeatherStore.isFormBlocked }
                type = 'submit'>
                { btnName }
            </button>
        </form>
    );
});
