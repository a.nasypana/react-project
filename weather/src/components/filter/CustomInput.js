export const CustomInput = (props) => {
    return (
        <p className = { props.className }>
            <label>{ props.label }</label>
            <input
                type = { props.type }
                value = { props.value }
                { ...props?.register } />
        </p>);
};

CustomInput.defaultProps = {
    type:      'number',
    className: 'custom-input',
};
