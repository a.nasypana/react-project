import { format } from 'date-fns';
import { uk } from 'date-fns/locale';

export const getDayOfWeek = (data) => {
    if (!data) {
        return '';
    }

    return format(new Date(data), 'EEEE', { locale: uk });
};


export const getData = (data) => {
    if (!data) {
        return '';
    }

    return format(new Date(data), 'd MMMM', { locale: uk });
};

export const selectDay = (days, selectId) => {
    if (days?.length === 0) {
        return {};
    }

    if (!selectId) {
        return days[ 0 ];
    }

    return days.find((d) => d.id === selectId);
};

export const arrangeDays = (arr) => {
    return arr?.length > 7 ? [...arr.slice(0, 7)] : [...arr];
};
