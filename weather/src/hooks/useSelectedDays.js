import { useStore } from './useStore';
import { arrangeDays, selectDay } from '../helpers/dataHelpers';

export const useSelectedDays = (days) => {
    const WeatherStore = useStore();
    const showDays = WeatherStore.isFiltered
        ? [...arrangeDays(WeatherStore.filteredDays(days))] : [...arrangeDays(days)];
    const day = selectDay(showDays, WeatherStore.selectedDayId);

    return {
        showDays,
        day,
    };
};
