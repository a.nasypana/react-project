import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { api } from '../api';
import { useStore } from './useStore';


export const useGetDays = () => {
    const WeatherStore = useStore();
    const {
        data, isLoading, isSuccess, isError,
    } = useQuery('days', api.getWeather, {
        onError(error) {
            console.log();
            WeatherStore.setError(error?.response?.data?.message);
        },
    });

    useEffect(() => {
        if (Array.isArray(data)) {
            if (!WeatherStore.selectedDayId) {
                WeatherStore.setSelectedDayId(data[ 0 ]?.id);
            }
        }
    }, [isSuccess]);

    return  {
        days: data ?? [],
        isLoading,
        isError,
    };
};
