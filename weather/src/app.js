// Components
import { useEffect } from 'react';
import { toast, ToastContainer, Slide } from 'react-toastify';
import { observer } from 'mobx-react-lite';
import {
    Header, CurrentWeather, Forecast, Filter,
} from './components';

// Instruments
import { useStore } from './hooks';

export const App = observer(() => {
    const WeatherStore = useStore();

    useEffect(() => {
        if (WeatherStore.errorMessage) {
            const notify = () => toast.error(WeatherStore.errorMessage, {
                position:        'top-right',
                autoClose:       5000,
                hideProgressBar: false,
                closeOnClick:    true,
                pauseOnHover:    true,
                draggable:       true,
                progress:        undefined,
            });
            notify();
            WeatherStore.resetError();
        }
    }, [WeatherStore.errorMessage]);


    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <main>
                <Filter />
                <Header />
                <CurrentWeather />
                <Forecast />
            </main>
        </>
    );
});

