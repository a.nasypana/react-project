// Core
import axios from 'axios';
import { WEATHER_URL } from './config';

export const api = Object.freeze({
    async getWeather() {
        const { data } =  await axios.get(WEATHER_URL);

        return data.data;
    },
});
