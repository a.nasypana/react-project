export const ROOT_URL = 'https://lab.lectrum.io';

export const WEATHER_URL = `${ROOT_URL}/rtx/api/forecast`;
