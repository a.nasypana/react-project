// Core
import { makeAutoObservable } from 'mobx';
import { computedFn } from 'mobx-utils';

export class WeatherStore {
    type = '';
    minTemperature = '';
    message = '';
    maxTemperature = '';
    isFiltered = false;
    selectedDayId = '';

    constructor() {
        this
            .filteredDays = computedFn((days) => {
                const filteredDays = days.filter((day) => {
                    const isCorrectType = this.type
                        ? this.type === day.type
                        : true;
                    const isCorrectMinTemperature = this.minTemperature
                        ? parseInt(this.minTemperature, 10) <= parseInt(day.temperature, 10)
                        : true;
                    const isCorrectMaxTemperature = this.maxTemperature
                        ? parseInt(this.maxTemperature, 10) >= parseInt(day.temperature, 10)
                        : true;

                    return (
                        isCorrectType
                    && isCorrectMinTemperature
                    && isCorrectMaxTemperature
                    );
                });

                if (filteredDays.length > 0) {
                    this.selectedDayId = filteredDays[ 0 ]?.id;
                }

                return filteredDays;
            });

        makeAutoObservable(this);
    }

    setType(type) {
        this.type = type;
    }

    setMinTemperature(temp) {
        this.minTemperature = temp;
    }

    setMaxTemperature(temp) {
        this.maxTemperature = temp;
    }

    applyFilter(filter) {
        if (filter.type) {
            this.type = filter.type;
        }

        if (filter.minTemperature) {
            this.minTemperature = filter.minTemperature;
        }

        if (filter.maxTemperature) {
            this.maxTemperature = filter.maxTemperature;
        }

        if (this.selectedDayId) {
            this.selectedDayId = '';
        }

        this.isFiltered = true;
    }

    get isFormBlocked() {
        return this.type === '' && this.minTemperature === '' && this.maxTemperature === '';
    }

    setSelectedDayId(id) {
        this.selectedDayId = id;
    }

    resetFilter() {
        this.maxTemperature = '';
        this.minTemperature = '';
        this.type = '';
        this.isFiltered = false;
        this.selectedDayId = '';
    }

    resetError() {
        this.message = '';
    }

    setError(newMessage) {
        this.message = newMessage;
    }

    get errorMessage() {
        return this.message;
    }
}

export const store = new WeatherStore();
